package com.gilbertopapa.cinesky.model;

/**
 * Created by GilbertoPapa on 20/07/2018.
 */

public class Move {


    private String id;
    private String title;
    private String overview;
    private String release_year;
    private String cover_url;
    private String [] backdrops_url = {};

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getRelease_year() {
        return release_year;
    }

    public void setRelease_year(String release_year) {
        this.release_year = release_year;
    }

    public String getCover_url() {
        return cover_url;
    }

    public void setCover_url(String cover_url) {
        this.cover_url = cover_url;
    }

    public String[] getBackdrops_url() {
        return backdrops_url;
    }

    public void setBackdrops_url(String[] backdrops_url) {
        this.backdrops_url = backdrops_url;
    }
}
