package com.gilbertopapa.cinesky.util;

/**
 * Created by GilbertoPapa on 19/07/2018.
 */

public class Configuration {
    private static final String URL = "https://sky-exercise.herokuapp.com/";

    public String getBaseUrl() {
        return this.URL;
    }
}
