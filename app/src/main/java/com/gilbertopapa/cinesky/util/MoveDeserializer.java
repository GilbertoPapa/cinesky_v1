package com.gilbertopapa.cinesky.util;

import com.gilbertopapa.cinesky.model.Move;
import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by GilbertoPapa on 19/07/2018.
 */

public class MoveDeserializer implements JsonDeserializer <Object>{
    @Override
    public Object deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        System.out.println(json.getAsString());
        List<Move> moveList = null;
        Type listType = new TypeToken<ArrayList<Move>>(){}.getType();
        moveList = new Gson().fromJson(json.getAsString(),listType);

        return moveList;


    }
}
