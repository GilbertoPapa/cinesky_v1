package com.gilbertopapa.cinesky.controller;

import android.util.Log;
import android.view.View;

import com.gilbertopapa.cinesky.R;
import com.gilbertopapa.cinesky.api.MoveApi;
import com.gilbertopapa.cinesky.model.Move;
import com.gilbertopapa.cinesky.util.Configuration;
import com.gilbertopapa.cinesky.util.MoveDeserializer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by GilbertoPapa on 20/07/2018.
 */

public class MoveController {

    private static final String TAG = ".";

    private Type listType = new TypeToken<ArrayList<Move>>() {
    }.getType();

    private Gson gson = new GsonBuilder().registerTypeAdapter(listType, new MoveDeserializer()).create();
    private Retrofit retrofit = new Retrofit
            .Builder()
            .baseUrl(new Configuration().getBaseUrl())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build();
    private MoveApi moveApi = retrofit.create(MoveApi.class);

    private List<Move> moves = null;

    public void GetMoves(String move, final View v,
                             final String title, final String idconf,
                             final String overview,final String release_year,final String cover_url,final String[] backdrops_url) {

        Call<List<Move>> call = moveApi.getMove("application/json", move);

        call.enqueue(new Callback<List<Move>>() {

            @Override
            public void onResponse(Call<List<Move>> call, Response<List<Move>> response) {
                moves = response.body();

                if(v.findViewById(R.id.listViewMoves)!=null){
                    List<HashMap<String,String>> orders = new ArrayList<HashMap<String,String>>();

                }else{
//-->
                }

            }
            @Override
            public void onFailure(Call<List<Move>> call, Throwable t) {
                Log.i(TAG, "");
            }
        });
    }


}
