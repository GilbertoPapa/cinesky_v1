package com.gilbertopapa.cinesky.api;

import com.gilbertopapa.cinesky.model.Move;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

/**
 * Created by GilbertoPapa on 20/07/2018.
 */

public interface MoveApi {

    @GET("api/Movies/{title}")
    Call<List<Move>> getMove(@Header("Content-Type") String content, @Path("title") String title);

}
